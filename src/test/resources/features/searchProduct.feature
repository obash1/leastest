Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios

  Scenario Outline: Get a particular product - positive scenario
    When User sends GET request with a '<productName>'
    Then user retrieves code 200
    And user sees the results displayed

    Examples:
      | productName |
      | apple       |
      | mango       |
      | tofu        |
      | water       |

#    Next scenario for now is failed, because not each title contains productName. But also, we don't know the requirements,
#    so let's imagine we have such a requirement
  Scenario Outline: Get a particular product and ensure the response title contains it - positive scenario
    When User sends GET request with a '<productName>'
    Then user retrieves code 200
    And user sees the '<productName>' displayed in the response title
    Examples:
      | productName |
      | apple       |
      | mango       |
      | tofu        |
      | water       |

    Scenario Outline: Get a particular (invalid) product - negative scenario
      When User sends GET request with a '<productName>'
      Then user retrieves code 404
      And error should be received
      Examples:
        | productName                                 |
        | car                                         |
        | null                                        |
        | 0                                           |
        | 1                                           |
        | %$&*)@!#$+=                                 |
        | itsalongvaluethatIwanttotestrightnowandhere |

    Scenario: Get an empty value- negative scenario
      When User sends GET request with a ''
      Then user retrieves code 401






