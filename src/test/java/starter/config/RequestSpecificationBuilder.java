package starter.config;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import org.apache.http.HttpHeaders;

public class RequestSpecificationBuilder {
    public static final String APP_BASE_URI = GlobalProperties.getInstance()
            .get("app.base.url");
    public static final String APP_BASE_PATH = GlobalProperties.getInstance()
            .get("app.base.path");

    public RequestSpecification buildRequestSpec() {
        return new RequestSpecBuilder()
                .setBaseUri(APP_BASE_URI)
                .setBasePath(APP_BASE_PATH)
                .addHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .build();
    }
}
