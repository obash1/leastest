package starter.stepDefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import starter.config.GlobalProperties;
import starter.config.RequestSpecificationBuilder;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.*;

public class SearchStepDefinitions {

    @Steps
    RequestSpecificationBuilder reqSpecBuilder = new RequestSpecificationBuilder();
    public String APP_SEARCH = GlobalProperties.getInstance().get("app.search");
    Response response;


    @When("User sends GET request with a {string}")
    public Response userSendsGETRequestWithAValidProductName(String productName) {
        response = SerenityRest.given().spec(reqSpecBuilder.buildRequestSpec())
                .when().get(APP_SEARCH, productName)
                .then().extract().response();
        return response;
    }

    @When("User sends DELETE request with a {string}")
    public void userSendsDELETERequestWithAValidProductName(String productName) {
        SerenityRest.given().spec(reqSpecBuilder.buildRequestSpec())
                .when().delete(APP_SEARCH, productName)
                .then().extract().response().asString();
    }

    @Then("user retrieves code {int}")
    public void userRetrievesCode(int code) {
        restAssuredThat(response -> response.statusCode(code));
    }

    @Then("user sees the results displayed")
    public void userSeesTheResultsDisplayedForProduct() {
        restAssuredThat(response -> response.body("title", is(notNullValue())));
    }

    @Then("user sees the {string} displayed in the response title")
    public void userSeesTheProductNameDisplayedInTheResponseTitle(String productName) {
        restAssuredThat(response -> response.body("title", contains(productName)));
    }

    @Then("error should be received")
    public void errorTrueShouldBeReceived() {
        restAssuredThat(response -> response.body("detail.error", equalTo(true)));
    }

}
