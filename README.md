#Write instructions in Readme file on how to install, run and write new tests

**Overview**

Leaseplan-example is a testing framework that includes API tests for GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} endpoint.
Framework: Java Serenity + Maven.
BDD tools: Cucumber/Gherkin.

**Project Structure**

All tests located in the src/test/java directory.

+test
+java
+starter
+config
GlobalProperties Class is a single thread-safe source of retrieving properties.
RequestSpecificationBuilder Class uses for building a general request specificaton.
+cucumber.Options
TestRunner Class - the runner class is CucumberWithSerenity - tests can be run via this Class. There are additional parameters added in @CucumberOptions Class that provides the root directory where the feature files can be found.
+stepDefinitions
SearchStepDefinitions Class contains step definitions for a Search feature.
+resources
+features
+search_product.feature  - feature file for a Search of a Product feature.
+logback-test.xml - configuration file that applications interact with to create log messages.
+serenity.conf - configuration file for Serenity.
+testing.properties - a file that contains base settings, such as base uri, path, endpoints, database credentials, authorization settings, etc.
+.gitlab-ci.yml - a YAML file that automatically runs whenever you push a commit to the server.
+pom.xml - it is an XML file that contains information about the project and configuration details used by Maven to build the project.

**Running Tests**
To run tests use TestRunner or run next command from the Terminal (incl. IDE Terminal):
$ mvn clean verify

**Writing new tests**

To write a new tests use next:
+src/test/resources/features - for describing new features; To add more tests to the Search of the Product feature use already existing feature file;
+src/test/java/starter/stepDefinitions - for adding step definitions; To add more step definitions to the Search of the Product feature use already existing Class.

**Reporting**

Reporting is performed with Serenity Test Report.
Serenity notifies in the log for the test events like Test start, test pass, test fail etc.
Once all your tests have run (with mvn clean verify command; locally), you can view the serenity test reports under the directory:

{Project-Home}target/site/serenity

index.html would have the consolidated test report.
For better appearance you can open the index.html report in a browser (copy absolute path to the index.html and open in the browser).

**CI/CD**

CI/CD on Gitlab is configured to the project. Settings may be found in the .gitlab-ci.yml file.
Currently, there are 2 stages available:
- build - compiling the app;
- test - running test scripts.
Once all the tests have run s inside a working CI/CD pipeline, the report is generated as a test artifact that is available to download.
After downloading a zip file unpack it and open index.html file in a browser to see the results of the test run.


#Briefly write in Readme what was refactored and why

- Added Config package that includes Request Specification Builder Class and Global Properties Class;
- Added Request Specification Builder Class - uses for building a general request specificaton.
- Added Global Properties Class - is a single thread-safe source of retrieving properties.
- TestRunner moved to cucumber.Options package - for better experience.
- Added glue = {"starter.stepDefinitions"} to CucumberOptions (TestRunner Class) - describes the location and path of the step definition file.
- Changed step definitions.
- Changed name of the feature file from post_product.feature to search_product.feature (it might have been also get_product.feature, because there is a GET endpoint).
- Changed wording in the feature file to more impersonal style (e.g. he\she changed to a 'user').
- 1 scenario divided by 3 scenarios (2 positive and 1 negative), added other scenario(s).
- Added .gitlab-ci.yml file with a valid configuration/ CI/CD configured.
- Changed README.md file.
- Project was cleaned up (all unnecessary data removed, such as Gradle, needless Classes, etc)  
- Cleaned up serenity.conf and serenity.properties files, pom file.  
- Set Html reporting - report is created after running tests locally, and after pipeline test run.
- Project uploaded on Gitlab.

# Getting started with Serenity and Cucumber
To find more information about Serenity visit https://serenity-bdd.github.io/theserenitybook/latest/maven.html

